#!/bin/bash

sleep 30

ip=$(gcloud compute instances describe section-instance --project=hpc4-gcp-renault --zone us-central1-a --format='get(networkInterfaces[0].accessConfigs[0].natIP)')

curl -f $ip

echo $?

if [ "$?" -eq "0" ]
then 
  echo "success reaching instance"
  exit 0 
else
  echo "failed reaching instance"
  echo $ip
  exit 1
fi
